MATLAB script for converting state space models into dynamical structure 
function models parameterized by (Q(s), P(s)).

A (strictly causal) state space model is given by 

        x^+ = Ax + Bu
        y = Cx
    
where 

        x in R^n is the current state of the input
        x^+ in R^n specifies the next state (which can either be the next 
                   timestep for discrete time or the time derivative for 
                   continuous time)
        u in R^m is the input
        y in R^p is the output
        A in R^{n x n}
        B in R^{n x m}
        C in R^{p x n}
        
This creates the DSF (Q(s), P(s)), defining the relationship between inputs and
outputs as

        Y(s) = Q(s)Y(s) + P(s)U(s)

where (defining T to be the space of rational polynomials in the complex 
variable s and also defining l to be the rank of C)

        U(s) is the laplace transform of the inputs
        Y(s) is the laplace transform of the outputs
        Q(s) in T^{l x l} 
        P(s) in T^{l x m}
        
## Installation ##

Clone to any directory on your machine.

(optional) add this directory to the MATLAB path following the directions 
[found here](https://www.mathworks.com/help/matlab/matlab_env/add-remove-or-reorder-folders-on-the-search-path.html).