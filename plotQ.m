function plotQ( Q , titles)
% Plots Q, where there is a link from y_j to y_i if Q_ij is not zero.
% Link weights are proportional to || Q_ij ||_{infty}.

[p, ~] = size(Q);
Qsize = zeros(p, p);

for i = 1 : p
    for j = 1 : p
        Qsize(i, j) = norm(Q(i,j), 'inf');
    end
end

% Normalize Qsize and Hsize
nQ = max(max(Qsize));
QNsize = Qsize / nQ;

ncolorize = @(f) (1 - f) * 0.5 + 0.5;

if nargin > 1
    SG = digraph(QNsize', titles);
else
    SG = digraph(QNsize');
end
fig = figure();
plt = SG.plot();
for i = 1 : p
    for j = 1 : p
        if i == j || QNsize(i,j) == 0
            continue
        end
        color = hsl2rgb([170/255, 1, ncolorize(QNsize(i,j))]);
        highlight(plt, j, i, 'LineWidth', QNsize(i,j), 'EdgeColor', color);
    end
end
set(gca, 'visible', 'off');
% set(gca,'position',[0 0 1 1],'units','normalized');
set(fig, 'Position', [0, 0, 1000, 1000]);

end

